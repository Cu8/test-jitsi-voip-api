const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const apn = require("apn");
const uuid = require("uuid");

const Pushy = require("pushy");

const PORT = 5000;
const MONGO_URL = "mongodb://localhost:27017/sip_test";
const VOIP_KEY_NAME = "jitsi-voip-services.p12";
const VOIP_KEY_PASS = "123456";
const PUSHY_SECRET_KEY =
  "79fbeea11bcb7e1278097f4b55b6d24704ed86f68e89724f1d5b01c922f37e5e";

const pushyAPI = new Pushy(PUSHY_SECRET_KEY);
const { v4: uuidv4 } = uuid;

const apnProvider = new apn.Provider({
  pfx: VOIP_KEY_NAME,
  passphrase: VOIP_KEY_PASS,
  // cert: VOIP_KEY_NAME,
  production: false,
});

const userSchema = new mongoose.Schema({
  username: String,
  voipToken: String,
  platform: String,
});

const User = mongoose.model("User", userSchema);
mongoose
  .connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // useCreateIndex: true,
  })
  .then(() => {
    console.log("DB connect successfully");
  });

const push = (token, payload, platform) => {
  if (platform === "ios") {
    const notification = new apn.Notification({
      alert: "Tekog Audio Call",
      sound: "default",
      pushType: "voip",
      payload,
    });
    return apnProvider.send(notification, token);
  } else {
    pushyAPI.sendPushNotification(payload, token, {}, () => {});
  }
};

const app = express();
app.use(cors());
app.use(express.json());

const api = express.Router();

api.post("/register", async (req, res, next) => {
  console.log(req.body);
  if (!req.body.username || !req.body.voipToken || !req.body.platform) {
    return res.status(400).end("Missing username or voipToken or platform");
  }

  let user = await User.findOne({ username: req.body.username });
  if (user) {
    user.voipToken = req.body.voipToken;
    user.platform = req.body.platform;
    user = await user.save();
    return res.send(user);
  } else {
    user = await User.create({
      username: req.body.username,
      voipToken: req.body.voipToken,
      platform: req.body.platform,
    });
    return res.send(user);
  }
});

api.post("/call", async (req, res, next) => {
  console.log(req.body);
  if (!req.body.username || !req.body.caller) {
    return res.status(400).end("Missing username or callerId");
  }
  const caller = await User.findOne({ username: req.body.caller });
  console.log("caller", caller);
  if (!caller) {
    return res.status(400).end("Invalid callerId");
  }

  const user = await User.findOne({ username: req.body.username });

  if (!user) {
    return res.status(400).end("Invalid username");
  }

  const uuid = uuidv4();
  const roomName = req.body.username.toLowerCase();
  try {
    await push(
      user.voipToken,
      {
        uuid: uuid,
        callerId: req.body.userId,
        callerName: req.body.caller,
        handle: "test_phone",
        extra: { roomName },
      },
      user.platform
    );
  } catch (e) {
    console.log("error", e);
  }

  res.send({ roomName, uuid });
});

api.post("/logout", async (req, res, next) => {
  if (!req.body.userId) {
    return res.status(400).end("Unknow user");
  }

  const user = await User.findById(req.body.userId);
  user.voipToken = "";
  await user.save();
  res.sendStatus(200);
});

app.get("/", (req, res) => {
  res.send("Test-sip-api is running");
});
app.use("/api", api);

app.listen(PORT);
console.log(PORT);
